const { Post } = require("../models");

module.exports = {
    getAllPost() {
        const posts = Post.findAll()
        return posts 
    },
    createPost(title, body){
        const posts = Post.create({
            title,
            body
        });
        return posts;
    },
    updatePost(post, postUpdate){
        return post.update(postUpdate)
    },
    deletePost(post){
        return post.destroy()
    },
    findKey(findIdPost){
        return Post.findByPk(findIdPost)
    }
}

  